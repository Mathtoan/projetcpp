#pragma once

#ifndef RENDER_AREA_HPP
#define RENDER_AREA_HPP

#include <QWidget>

#include "../header/terrain.hpp"

//forward declaration of QPixmap
class QPixmap;
class render_area : public QWidget
{
    Q_OBJECT
    public:
        //Constructor
        render_area(QWidget *parent = nullptr);
        ~render_area();

        //Getter & setter
        const Terrain& getTerrain() const;
        Terrain& setTerrain();

        const int& getSelect() const;
        int& setSelect();

        //Method
        void interfaceMove(int);

        void BFS();
        void DFS();
        void moveToDestination();

    protected:
        void paintEvent(QPaintEvent *event);
        void mousePressEvent(QMouseEvent* event);
        void keyPressEvent ( QKeyEvent * event );
    private:

        Terrain _terrain;
        int _select;
        //Destination's coordonates, set as -1 by default when not selected yet
        int _destSelectX;
        int _destSelectY;

};

#endif
