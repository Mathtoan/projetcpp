#ifndef OBSTACLE_HPP
#define OBSTACLE_HPP

#include "cell.hpp"

class Obstacle: public Cell
{
    public:
        //Constructor
        Obstacle();
        Obstacle(int, int);

        //Method
        bool canBeTravelled() const override;
    private:
};

#endif