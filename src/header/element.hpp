#ifndef ELEMENT_HPP
#define ELEMENT_HPP

#include <vector>
#include <iostream>

template <typename T>
class Element
{
    public:
        //Constructor
        Element()=default;
        Element(int, int);

        //Getter & setter
        const int& getX() const;
        int& setX();

        const int& getY() const;
        int& setY();

        const std::vector<T>& getV4() const;
        std::vector<T>& setV4();

    protected:
        int _x;
        int _y;
        std::vector<T> _V4;

};

template <typename T>
Element<T>::Element(int x, int y)
    :_x(x), _y(y), _V4({nullptr, nullptr, nullptr, nullptr})
{}

template <typename T>
const std::vector<T>& Element<T>::getV4() const
{
    return _V4;
}

template <typename T>
std::vector<T>& Element<T>::setV4()
{
    return _V4;
}

template <typename T>
const int& Element<T>::getX() const
{
    return _x;
}

template <typename T>
const int& Element<T>::getY() const
{
    return _y;
}

template <typename T>
int& Element<T>::setX()
{
    return _x;
}

template <typename T>
int& Element<T>::setY()
{
    return _y;
}

#endif