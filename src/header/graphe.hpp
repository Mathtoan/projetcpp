#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <vector>
#include <iostream>

template <typename CELL>
class Graphe
{
    public:
        //Constructor
        Graphe()=default;
        Graphe(int, int, const std::vector<CELL>&);

        //Getter & Setter
        const int& lengthX() const;
        int& lengthX();

        const int& lengthY() const;
        int& lengthY();

        const std::vector<CELL>& getCell_map() const;
        std::vector<CELL>& setCell_map();
        
        //Method
        int from_xy_to_i(int x, int y) const;
        std::vector<int> from_i_to_xy(int i) const;
    protected:
        int _lengthX;
        int _lengthY;
        std::vector<CELL> _cell_map;
};

template <typename CELL>
Graphe<CELL>::Graphe(int x, int y, const std::vector<CELL>& cell_map)
    :_lengthX(x), _lengthY(y), _cell_map(cell_map)
{}

template <typename CELL>
const int& Graphe<CELL>::lengthX() const
{
    return _lengthX;
}

template <typename CELL>
const int& Graphe<CELL>::lengthY() const
{
    return _lengthY;
}

template<typename CELL>
const std::vector<CELL>& Graphe<CELL>::getCell_map() const{
    return _cell_map;
}

template <typename CELL>
int& Graphe<CELL>::lengthX()
{
    return _lengthX;
}

template <typename CELL>
int& Graphe<CELL>::lengthY()
{
    return _lengthY;
}
template<typename CELL>
std::vector<CELL>& Graphe<CELL>::setCell_map(){
    return _cell_map;
}

template <typename CELL>
int Graphe<CELL>::from_xy_to_i(int x, int y) const{
    int i = y*_lengthX + x;
    return i;
}

template <typename CELL>
std::vector<int> Graphe<CELL>::from_i_to_xy(int i) const{
    int x = i%_lengthX;
    int y = i/_lengthX;

    std::vector<int> xy = {x, y};
    return xy;
}

#endif