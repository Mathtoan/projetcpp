#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "element.hpp"
#include "cell.hpp"
#include <vector>

class Character: public Element<Cell*>
{
    public:
        //Constructor
        Character()=default;
        Character(int, int);

        //Method
        bool elementarMove(int dir);
};

#endif