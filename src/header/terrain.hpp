#ifndef TERRAIN_HPP
#define TERRAIN_HPP

#include "graphe.hpp"
#include "cell.hpp"
#include "character.hpp"

class Terrain: public Graphe<Cell*>
{
    public:
        //Constructor
        Terrain()=default;
        Terrain(int, int, const std::vector<Cell*>&, const std::vector<Character>&);

        //Getter & setter
        const std::vector<Character>& getCharacter() const;
        std::vector<Character>& setCharacter();

        const std::vector<std::vector<std::vector<int>>>& getCurrentTree() const;

        //Method
        void calcV4(Character&);
        void calcV4(Cell*);

        void BFS(int i);
        void DFS(int id_cell, int level, int dir, int parent);
        void initCurrentTree();

        bool moveCharacter(int i, int dir);
        void consoleDisplay();

    private:
        std::vector<Character> _character;
        std::vector<std::vector<std::vector<int>>> _currentTree;
};

std::ostream& operator<<(std::ostream& s, const std::vector<Cell*>& cell_map);
std::ostream& operator<<(std::ostream& s, const std::vector<Character>& cell_map);

#endif