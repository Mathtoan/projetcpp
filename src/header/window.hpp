#pragma once

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <QMainWindow>
#include <QtGui>
#include <QWidget>

//forward declaration
namespace Ui{
class MainWindow;
}
class render_area;


class window: public QMainWindow
{
    Q_OBJECT

    public:

        window(QWidget *parent=NULL);
        ~window();

    private slots:
        void action_quit();
        void BFS_button();
        void DFS_button();
        void move_to();
        void moveLeft();
        void moveUp();
        void moveRight();
        void moveDown();
        void selectCharacter();

    private:

        Ui::MainWindow *ui;
        render_area *render;

};

#endif
