#ifndef CELL_HPP
#define CELL_HPP

#include "element.hpp"
#include <iostream>

class Cell: public Element<Cell*>
{
    public:
        //Constructor
        Cell();
        Cell(int, int);

        //Getter & setter
        const bool& getFreeState() const;
        bool& setFreeState();

        const bool& getAddedToTreeState() const;
        bool& setAddedToTreeState();

        //Method
        virtual bool canBeTravelled() const {return true;} 
    private:
        bool _isFree;
        bool _isAddedToTree;
};

std::ostream& operator<<(std::ostream& s, Cell const & cell);

#endif