#include "../header/window.hpp"
#include "../header/render_area.hpp"

#include "ui_mainwindow.h"

#include <iostream>

window::window(QWidget *parent)
    :QMainWindow(parent),ui(new Ui::MainWindow),render(new render_area)
{
    //setup the graphical interface to the current widget
    ui->setupUi(this);

    //Attach the render_area window to the widget
    ui->layout_scene->addWidget(render);

    //connect signals
    connect(ui->quit,SIGNAL(clicked()),this,SLOT(action_quit()));
    
    connect(ui->BFS,SIGNAL(clicked()),this,SLOT(BFS_button()));
    connect(ui->DFS,SIGNAL(clicked()),this,SLOT(DFS_button()));
    connect(ui->move_button,SIGNAL(clicked()),this,SLOT(move_to()));

    connect(ui->left, SIGNAL(clicked()), this, SLOT(moveLeft()));
    connect(ui->up, SIGNAL(clicked()), this, SLOT(moveUp()));
    connect(ui->right, SIGNAL(clicked()), this, SLOT(moveRight()));
    connect(ui->down, SIGNAL(clicked()), this, SLOT(moveDown()));

    ui->select->setMaximum(render->getTerrain().getCharacter().size()-1);
    connect(ui->select, SIGNAL(editingFinished()), this, SLOT(selectCharacter()));
}


window::~window()
{

}

void window::action_quit()
{
    close();
}

void window::BFS_button()
{
    render->BFS();
}

void window::DFS_button()
{
    render->DFS();
}

void window::move_to()
{
    if(ui->BFS->isChecked())
    {
        render->BFS();
    }
    else if(ui->DFS->isChecked())
    {
        render->DFS();
    }
    render->moveToDestination();
}

void window::selectCharacter()
{
    render->setSelect() = ui->select->value();
}

void window::moveLeft()
{
    render->interfaceMove(0);
}
void window::moveUp()
{
    render->interfaceMove(1);
}
void window::moveRight()
{
    render->interfaceMove(2);
}
void window::moveDown()
{
    render->interfaceMove(3);
}
