
#include "../header/render_area.hpp"
#include "../header/obstacle.hpp"
#include "../header/character.hpp"
#include "../header/cell.hpp"

#include <iostream>
#include <time.h>

#include <QPixmap>
#include <QPainter>
#include <QMouseEvent>
#include <QPointF>
#include <QRectF>
#include <list>
#include <unistd.h>

QColor col[6] = {Qt::gray, Qt::blue, Qt::red, Qt::darkGreen, Qt::green, Qt::black};


render_area::render_area(QWidget *parent)
    :QWidget(parent), _select(0), _destSelectX(-1), _destSelectY(-1)
{
    setFocusPolicy(Qt::StrongFocus); //Activate keyboard event
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);

    //initialisation _terrain
    int lenX = 24;
    int lenY = 11;

    std::srand(time(NULL));

    std::vector<Cell*> cell_map;
    std::vector<Character> character;

    //Cell generation
    for(int i=0; i<lenX*lenY;i++)
    {
        int N = std::rand() % 3;
        Cell* cell;

        if(N==0) {cell = new Obstacle();}
        else {cell = new Cell();}

        cell_map.push_back(cell);
    }
    
    //Character generation
    int nbCharacters = 3;
    for(int j = 0; j<nbCharacters; j++)
    {   
        int Nx = std::rand() % lenX;
        int Ny = std::rand() % lenY;
        Character init_c1(Nx,Ny);
        character.push_back(init_c1);
    }
    Terrain terrain(lenX, lenY, cell_map, character);
    _terrain = terrain;
}

render_area::~render_area()
{}

const Terrain& render_area::getTerrain() const
{
    return _terrain;
}

Terrain& render_area::setTerrain()
{
    return _terrain;
}

const int& render_area::getSelect() const
{
    return _select;
}

int& render_area::setSelect()
{
    return _select;
}

void render_area::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    std::vector<Cell*> cell_map = _terrain.getCell_map();
    std::vector<Character> character = _terrain.getCharacter();

    //The drawing pen with its properties
    QPen pen;
    pen.setWidth(1.0);
    pen.setColor(col[5]);
    painter.setPen(pen);

    QBrush brush = painter.brush();
    
    //Drawing every cell
    for(const auto& el:cell_map)
    {
        //Red : obstacle
        if(dynamic_cast<Obstacle*>(el)!= nullptr)
        {
            brush.setColor(col[2]);
        }
        //Green : Can be reached by the character
        else if(el->getAddedToTreeState())
        {
            brush.setColor(col[4]);
        }
        //Grey : can't be reached by the character
        else
        {
            brush.setColor(col[0]);
        }
        
        brush.setStyle(Qt::SolidPattern);
        painter.setBrush(brush);

        int x = el->getX();
        int y = el->getY();
        
        //Cells drawn as a square
        QPointF points[4] = {
            QPointF(x*30.0, y*30.0),
            QPointF((x+1)*30.0, y*30.0),
            QPointF((x+1)*30.0, (y+1)*30.0),
            QPointF(x*30.0, (y+1)*30.0)
        };
        painter.drawConvexPolygon(points, 4);
    }
    pen.setColor(col[1]);
    painter.setPen(pen);

    //Drawing every character as a cross
    for(auto& el:character)
    {
        int x = el.getX();
        int y = el.getY();

        QRectF r1(((float)x+0.5)*30., y*30, 0., 115.);
        QRectF r2(x*30., ((float)y+0.5)*30., 30., 0.);


        painter.drawArc(r1, 30*16, 120*16);
        painter.drawArc(r2, 30*16, 120*16);
    }

    //Drawing the destination selector as a circle
    if(_destSelectX != -1 and _destSelectY != -1)
    {
        brush.setStyle(Qt::NoBrush);
        painter.setBrush(brush);
        painter.drawEllipse(((float)_destSelectX)*30., ((float)_destSelectY)*30., 30., 30.);
    }
}

//Manual move with interface buttons
void render_area::interfaceMove(int dir)
{
    _terrain.moveCharacter(_select, dir);
    repaint();
}

//Manual move with keyboard's arrows
void render_area::keyPressEvent(QKeyEvent* event)
{
    if(event->key() == Qt::Key_Left) _terrain.moveCharacter(_select, 0);
    else if(event->key() == Qt::Key_Up) _terrain.moveCharacter(_select, 1);
    else if(event->key() == Qt::Key_Right) _terrain.moveCharacter(_select, 2);
    else _terrain.moveCharacter(_select, 3);
    repaint();
}

//Selection of the destination
void render_area::mousePressEvent(QMouseEvent* event)
{
    int clickX = event->x();
    int clickY = event->y();

    int x = clickX/30;
    int y = clickY/30;

    if(x<_terrain.lengthX() and y<_terrain.lengthY())
    {
        int k = _terrain.from_xy_to_i(x, y);

        if(dynamic_cast<Obstacle*>(_terrain.getCell_map()[k])==nullptr)
        {
            _destSelectX = x;
            _destSelectY = y;
            repaint();
        }
    }
}

//Calculation of the BFS tree
void render_area::BFS()
{
    _terrain.initCurrentTree();
    _terrain.BFS(_select);
    repaint();
}

//Calculation of the DFS tree
void render_area::DFS()
{
    const auto& c = _terrain.getCharacter()[_select];
    int id_cell = _terrain.from_xy_to_i(c.getX(), c.getY());
    _terrain.initCurrentTree();
    _terrain.DFS(id_cell, 0,0,0);
    repaint();
}

//Automatic move with one of the calculated trees
void render_area::moveToDestination()
{
    if(_destSelectX==-1 or _destSelectY==-1)
    {
        std::cout<<"please choose a destination"<<std::endl;
    }
    else
    {
        int id = _terrain.from_xy_to_i(_destSelectX, _destSelectY);
        
        if(not(_terrain.getCell_map()[id]->getAddedToTreeState()))
        {
            std::cout<<"Can't reach this point"<<std::endl;
        }
        else
        {
            //Looking for the destination level in the tree
            for(int i=0;i<(int)_terrain.getCurrentTree().size();i++)
            {
                const auto& level = _terrain.getCurrentTree()[i];
                bool exitLoop1 = false;

                //Looking for the destination in the tree level
                for(const auto& el:level)
                {
                    //Destination found : creation of the path
                    if(id == el[0])
                    {
                        std::list<int> dir = {el[1]};
                        int parent = el[2];
                        for(int j = i; j>1; j--)
                        {
                            dir.push_front(_terrain.getCurrentTree()[j-1][parent][1]);
                            parent = _terrain.getCurrentTree()[j-1][parent][2];
                        }

                        for(auto& move:dir)
                        {
                            int character_x = _terrain.getCharacter()[_select].getX();
                            int character_y = _terrain.getCharacter()[_select].getY();
                            if(character_x != _destSelectX or character_y != _destSelectY)
                            {
                                _terrain.moveCharacter(_select, move);
                                repaint();
                                int t = 333; //unit:ms
                                usleep(t*1000);
                            }
                        }
                        exitLoop1 = true;
                        break;
                    }
                    
                }
                if(exitLoop1) break;
            }
        }
    }
}
