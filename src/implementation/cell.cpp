#include "../header/cell.hpp"

Cell::Cell()
    :_isFree(true), _isAddedToTree(false)
{}

Cell::Cell(int x, int y)
    :Element(x, y), _isFree(true), _isAddedToTree(false)
{}

std::ostream& operator<<(std::ostream& s, Cell const & cell)
{
    s<<"x : "<<cell.getX()<<"; y : "<<cell.getY();
    return s;
}

const bool& Cell::getFreeState() const
{
    return _isFree;
}

bool& Cell::setFreeState()
{
    return _isFree;
}

const bool& Cell::getAddedToTreeState() const
{
    return _isAddedToTree;
}

bool& Cell::setAddedToTreeState()
{
    return _isAddedToTree;
}