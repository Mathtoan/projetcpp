#include "../header/terrain.hpp"
#include "../header/obstacle.hpp"

Terrain::Terrain(int x, int y, const std::vector<Cell*>& cell_map, const std::vector<Character>& character)
    :Graphe(x, y, cell_map), _character(character)
{   
    for(int i = 0;i<(int) cell_map.size();i++)
    {
        std::vector<int> xy_cell = from_i_to_xy(i);

        Cell* cell = _cell_map[i];
        cell->setX() = xy_cell[0];
        cell->setY() = xy_cell[1];
    }

    for(auto& el:_cell_map)
    {
        calcV4(el);
    }
    // Check if character is in an obstacle and if the cell is free
    for(auto& el : _character)
    {
        int x = el.getX();
        int y = el.getY();

        int k = from_xy_to_i(x, y);
        int dim = lengthX()*lengthY();

        while(not(_cell_map[k]->canBeTravelled()) || not(_cell_map[k]->getFreeState()))
        {
            k+=1;
            k%=dim;
        }
        std::vector<int> xy_character = from_i_to_xy(k);
        _cell_map[k]->setFreeState() = false;
        el.setX() = xy_character[0];
        el.setY() = xy_character[1];
        calcV4(el);
    }
}

const std::vector<Character>& Terrain::getCharacter() const
{
    return _character;
}

std::vector<Character>& Terrain::setCharacter()
{
    return _character;
}

const std::vector<std::vector<std::vector<int>>>& Terrain::getCurrentTree() const
{
    return _currentTree;
}

void Terrain::calcV4(Character& character)
{

        int x = character.getX();
        int y = character.getY();

        std::vector<Cell*> newV4 = {nullptr, nullptr, nullptr, nullptr};

        if(x!=0)
        {
            int k = from_xy_to_i(x-1, y);
            newV4[0] = _cell_map[k];
        }

        if(y!=0)
        {
            int k = from_xy_to_i(x, y-1);
            newV4[1] = _cell_map[k];
        }
        if(x!=_lengthX-1)
        {
            int k = from_xy_to_i(x+1, y);
            newV4[2] = _cell_map[k];
        }

        if(y!=_lengthY-1)
        {
            int k = from_xy_to_i(x, y+1);
            newV4[3] = _cell_map[k];
        }

        character.setV4() = newV4;
}

void Terrain::calcV4(Cell* cell)
{

        int x = cell->getX();
        int y = cell->getY();

        std::vector<Cell*> newV4 = {nullptr, nullptr, nullptr, nullptr};

        if(x!=0)
        {
            int k = from_xy_to_i(x-1, y);
            if(dynamic_cast<Obstacle*>(_cell_map[k])==nullptr)
            {
                newV4[0] = _cell_map[k];
            }
        }

        if(y!=0)
        {
            int k = from_xy_to_i(x, y-1);
            if(dynamic_cast<Obstacle*>(_cell_map[k])==nullptr)
            {
                newV4[1] = _cell_map[k];
            }
        }
        if(x!=_lengthX-1)
        {
            int k = from_xy_to_i(x+1, y);
            if(dynamic_cast<Obstacle*>(_cell_map[k])==nullptr)
            {
                newV4[2] = _cell_map[k];
            }
        }

        if(y!=_lengthY-1)
        {
            int k = from_xy_to_i(x, y+1);
            if(dynamic_cast<Obstacle*>(_cell_map[k])==nullptr)
            {
                newV4[3] = _cell_map[k];
            }
        }

        cell->setV4() = newV4;
}

bool Terrain::moveCharacter(int select, int dir)
{
    Character& character = _character[select];
    
    int old_X = character.getX();
    int old_Y = character.getY();

    std::cout<<"C"<<select<<" : ";
    if(character.elementarMove(dir))
    {
        int old_k = from_xy_to_i(old_X, old_Y);

        int new_X = character.getX();
        int new_Y = character.getY();
        int new_k = from_xy_to_i(new_X, new_Y);

        _cell_map[old_k]->setFreeState() = true;
        _cell_map[new_k]->setFreeState() = false;

        calcV4(character);

        return true;
    }
    else
    {
        return false;
    }
}

void Terrain::consoleDisplay()
{
    int i = 0;
    for(const auto& el : _cell_map){
        const int& x_cells = el->getX();
        const int& y_cells = el->getY();

        std::cout<<"[";
        if(dynamic_cast<Obstacle*>(el)!= nullptr)
        {
            std::cout<<"O";
        }
        else
        {
            std::cout<<"C";
        }
        std::cout<<";";
        
        if(i<=(int)_character.size())
        {
            const int& x_character = _character[i].getX();
            const int& y_character = _character[i].getY();

            if(x_cells==x_character && y_cells==y_character)
            {
                std::cout<<"+";
                i++;
            }
            else
            {
                std::cout<<" ";
            }
        }
        std::cout<<"]";
        if(x_cells==lengthX()-1)
        {
            std::cout<<std::endl;
        }
    }
}

void Terrain::initCurrentTree()
{
    for(auto& cell:_cell_map)
    {
        cell->setAddedToTreeState() = false;
    }
}
void Terrain::BFS(int i)
{
    for(auto& cell:_cell_map)
    {
        cell->setAddedToTreeState() = false;
    }
    std::vector<std::vector<int>> null;
    int id_cell = from_xy_to_i(_character[i].getX(), _character[i].getY());
    auto& Cell = _cell_map[id_cell];
    Cell->setAddedToTreeState() = true;
    _currentTree = {{{id_cell}}};

    int level = 0;
    while(_currentTree[level] != null)
    {
        level++;
        std::vector<std::vector<int>> level_content;
        for(int fromElement=0; fromElement<(int)_currentTree[level-1].size(); fromElement++) //level_content creation
        {
            id_cell = _currentTree[level-1][fromElement][0];
            for(int dir=0; dir<4; dir++) //Items of level_content
            {
                const auto& el = _cell_map[id_cell]->getV4()[dir];
                if(el!=nullptr)
                {
                    if(el->getFreeState() and not(el->getAddedToTreeState()))
                    {
                        int x_cell = el->getX();
                        int y_cell = el->getY();
                        int id_cell1 = from_xy_to_i(x_cell, y_cell);
                        level_content.push_back({id_cell1, dir, fromElement});

                        _cell_map[id_cell1]->setAddedToTreeState() = true;
                    }
                }
            }
        }
        _currentTree.push_back(level_content);
    }
    _currentTree.pop_back();
}

void Terrain::DFS(int id_cell, int level, int dir, int parent)
{
    int newParent;
    auto& Cell = _cell_map[id_cell];
    Cell->setAddedToTreeState() = true;
    if(level == 0) //Initialization of DFS
    {
        _currentTree = {{{id_cell}}};
    }
    else if((int)_currentTree.size()<level+1) //If new level
    {
        std::vector<std::vector<int>> Level = {{id_cell, dir, parent}};
        _currentTree.push_back(Level);
        newParent = 0;
    }
    else //If level already created
    {
        _currentTree[level].push_back({id_cell, dir, parent});
        newParent = (int)_currentTree[level].size()-1;
    }
    
    int newDir = 0;
    for(const auto& el:Cell->getV4())
    {
        if(el!=nullptr and not(el->getAddedToTreeState()) and el->getFreeState())
        {
            int x = el->getX();
            int y = el->getY();
            int new_id_cell = from_xy_to_i(x, y);

            DFS(new_id_cell, level+1, newDir, newParent);
        }
        newDir++;
    }
}

std::ostream& operator<<(std::ostream& s, const std::vector<Cell*>& cell_map)
{
    for(int i = 0; i<(int) cell_map.size(); i++){
        int x = cell_map[i]->getX();
        int y = cell_map[i]->getY();

        s<<"i : "<<i<<"; x : "<<x<<"; y : "<<y;
        s <<"; can be travelled : "<<cell_map[i]->canBeTravelled()<<std::endl;
    }
    return s;
}

std::ostream& operator<<(std::ostream& s, const std::vector<Character>& character){
    for(const auto& i : character){
        int x = i.getX();
        int y = i.getY();
        s<<"x : "<<x<<"; y : "<<y<<std::endl;
    }
    return s;
}