#include "../header/character.hpp"
#include <stdlib.h>

Character::Character(int x, int y)
    :Element(x, y)
{}

bool Character::elementarMove(int dir)
{   
    if (_V4[dir]==nullptr)
    {
        std::cout<<std::endl<<"No neighbour, can't move"<<std::endl;
        return false;
    }
    else if(not(_V4[dir]->canBeTravelled()))
    {
        std::cout<<std::endl<<"Cell is an obstable, can't move"<<std::endl;
        return false;
    }
    else if(not(_V4[dir]->setFreeState()))
    {
        std::cout<<std::endl<<"Cell occuped, can't move"<<std::endl;
        return false;
    }
    else
    {
        const int& x_cell = _V4[dir]->getX();
        const int& y_cell = _V4[dir]->getY();
        _x = x_cell;
        _y = y_cell;
        std::cout<<"has moved to (x : "<<_x<<", y : "<<_y<<")"<<std::endl;
        return true;
    }  
}