#include "../header/obstacle.hpp"

Obstacle::Obstacle()
    :Cell()
{}

Obstacle::Obstacle(int x, int y)
    :Cell(x, y)
{}

bool Obstacle::canBeTravelled() const
{
    return false;
}