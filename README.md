# Projet CPP

Projet C++ 4ETI-IMI 2019-2020 (Gitlab [ici](https://gitlab.com/Mathtoan/projetcpp))

## Requirement/version
### Libraries particulière
- **Qt** -  4.8.7
### Pour la compilation
- ```gcc``` - 7.4.0
- ```make``` -  4.1
- ```cmake``` - 3.10.2
- ```qmake``` -  2.01a

## Instructions de compilation
1. Créer un fichier ```build/``` à la racine du dossier du projet et s'y placer.
2. Executer le CMakeList avec ```cmake ../```
3. Executer le makefile avec ```make```
4. Lancer l'exécutable avec ```./pgm```